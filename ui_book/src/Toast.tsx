/** @jsxImportSource @emotion/react */
import { jsx, Global, css } from "@emotion/react";
import * as React from "react";

import toaster, { Position } from "../../src/Alert";
// import { Alert, AlertIntentions } from "./Alert";
// import { Theme } from "./Theme";
// import { useTheme, ThemeProvider } from "./Theme/Providers";
import {
  Toolbar,
  Navbar,
  useTheme,
  IconButton,
  Button,
  Tabs,
  Tab,
  Layer,
  TabPanel,
  MenuList,
  MenuItem,
  Tooltip,
  ResponsivePopover,
  IconChevronDown,
  IconPlus,
  DarkMode,
  LightMode,
  Pager, IconArchive, ButtonRefComp, DarkRefMode,
  useMMenuBarOcup, TwoHalfFrame, ResponderEvent, StateType, Theme,
  Alert, AlertIntentions,ThemeProvider
} from "customize-easy-ui-component";




const toastStyles = css`
  .Toaster__message-wrapper {
    padding: 8px;
    text-align: left;
  }
`;

interface RenderArgs {
  id: string;
  onClose: () => void;
}

interface Toast {
  position?: keyof typeof Position;
  duration?: number | null;
  title?: string;
  subtitle?: string;
  theme?: Theme;
  intent?: AlertIntentions;
  render?: (options: RenderArgs) => React.ReactNode;
}

/**
 * We export toast as a hook because it allows us to consume
 * the current theme context that it's in and pass that
 * onto our render function
 报错来源： ReactDOM.render is no longer supported in React 18. Use createRoot instead. Until you switch to the new API, your app will behave as if it's running React 17.
# "react-toastnotes"包 ？竟然被我遗忘了。
 */

export function useToast() {
  const theme = useTheme();

  function notify({
    position ="bottom",
    duration,
    render,
    title,
    subtitle,
    intent
  }: Toast) {
    const options = {
      position,
      duration
    };

    if (render) {
      return toaster.notify(
        ({ onClose, id }) => (
          <React.Fragment>
            <Global styles={toastStyles} />
            <ThemeProvider theme={theme}>
              {render({ onClose, id })}
            </ThemeProvider>
          </React.Fragment>
        ),
        options
      );
    }

    toaster.notify(
      ({ onClose, id }) => (
        <React.Fragment>
          <Global styles={toastStyles} />
          <ThemeProvider theme={theme}>
            <Alert
              id={String(id)}
              title={title}
              component="div"
              elevation={"md"}
              subtitle={subtitle}
              intent={intent}
              onRequestClose={onClose}
            />
          </ThemeProvider>
        </React.Fragment>
      ),
      options
    );
  }

  return notify;
}
