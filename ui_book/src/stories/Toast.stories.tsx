/** @jsxImportSource @emotion/react */
import { jsx } from "@emotion/react";
import * as React from "react";
// import { Button } from "../Button";
import { storiesOf } from "@storybook/react";
import { useToast } from "../Toast";
// import { ToggleDarkMode } from "./ToggleDarkMode";
import {
    Toolbar,
    Navbar,
    useTheme,
    IconButton,
    Button,
    Tabs,
    Tab,
    Layer,
    TabPanel,
    MenuList,
    MenuItem,
    Tooltip,
    ResponsivePopover,
    IconChevronDown,
    IconPlus,
    DarkMode,
    LightMode,
    Pager, IconArchive, ButtonRefComp, DarkRefMode,
    useMMenuBarOcup, TwoHalfFrame, ResponderEvent, StateType, Theme,
    Alert, AlertIntentions,ThemeProvider,
} from "customize-easy-ui-component";


function Example() {
  const toast = useToast();
  return (
    <Button
      onClick={() => {
        toast({
          duration: 2000,
          title: "Hello world",
          subtitle: "Excepteur exercitation 停留 2500 ms.",
          position: "top"
        });
      }}
    >
      Show toast
    </Button>
  );
}

export const ToastStories = storiesOf("Toast", module).add("basic", () => {
  return (
    <div css={{ PADDING: "3rem" }}>
      <Example />
    </div>
  );
});
